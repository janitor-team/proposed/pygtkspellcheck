Source: pygtkspellcheck
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Raphaël Hertzog <hertzog@debian.org>
Build-Depends: debhelper-compat (= 13),
               dh-python,
               python3,
               python3-sphinx,
               python3-enchant,
               python3-gi,
               gir1.2-gtk-3.0,
Standards-Version: 3.9.8
Homepage: https://pygtkspellcheck.readthedocs.org/
Vcs-Browser: https://salsa.debian.org/python-team/packages/pygtkspellcheck
Vcs-Git: https://salsa.debian.org/python-team/packages/pygtkspellcheck.git

Package: python3-gtkspellcheck
Architecture: all
Depends: ${python3:Depends}, ${misc:Depends}, python3-enchant, python3-gi, gir1.2-gtk-3.0
Recommends: iso-codes
Suggests: python-gtkspellcheck-doc
Description: Python 3 spellchecking library for GTK+ based on Enchant
 It supports both GTK+'s Python bindings, PyGObject and PyGtk, and for both
 Python 2 and 3 with automatic switching and binding autodetection. For
 automatic translation of the user interface it can use GEdit's translation
 files.
 .
 This package contains the Python 3 version with support for PyGObject.

Package: python-gtkspellcheck-doc
Architecture: all
Section: doc
Depends: ${sphinxdoc:Depends}, ${misc:Depends}
Description: Python GTK+ Spellcheck common documentation
 It supports both GTK+'s Python bindings, PyGObject and PyGtk, and for both
 Python 2 and 3 with automatic switching and binding autodetection. For
 automatic translation of the user interface it can use GEdit's translation
 files.
 .
 This is the common documentation package.
